extern crate scientist;

use scientist::Experiment;

fn main() {
    let candidate_s = String::from("abc");
    // create an extra copy so that both can be moved
    let control_s = candidate_s.clone();

    let result = Experiment::new()
        .candidate(Box::new(move || {
            println!("running candidate");
            let mut updated = candidate_s.clone();
            updated.push_str("def");
            updated
        }))
        .control(Box::new(move || {
            println!("running control");
            let mut updated = control_s.clone();
            updated.push_str("xyz");
            updated
        }))
        .run();

    println!("final (control) result: {:?}", result);
}
