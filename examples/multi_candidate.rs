extern crate scientist;

use scientist::Experiment;

fn main() {
    let result = Experiment::new()
        .named_candidate(
            "candidate_1",
            Box::new(|| {
                println!("running candidate_1");
                1
            }),
        )
        .named_candidate(
            "candidate_2",
            Box::new(|| {
                println!("running candidate_2");
                2
            }),
        )
        .control(Box::new(|| {
            println!("running control");
            3
        }))
        .run();

    println!("final (control) result: {:?}", result);
}
