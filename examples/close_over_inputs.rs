extern crate scientist;

use scientist::Experiment;

fn main() {
    let a = 3;
    let b = 4;

    let result = Experiment::new()
        .candidate(Box::new(move || {
            println!("running candidate");
            a * 2 + b
        }))
        .control(Box::new(move || {
            println!("running control");
            a * b
        }))
        .run();

    println!("final (control) result: {:?}", result);
}
