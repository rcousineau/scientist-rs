extern crate scientist;

use scientist::Experiment;

fn main() {
    println!("* failing experiment *");
    let result = Experiment::new()
        .candidate(Box::new(|| {
            println!("running candidate");
            3
        }))
        .control(Box::new(|| {
            println!("running control");
            4
        }))
        .run();

    println!("final (control) result: {:?}", result);

    println!("* passing experiment *");
    let result = Experiment::new()
        .candidate(Box::new(|| {
            println!("running candidate");
            4
        }))
        .control(Box::new(|| {
            println!("running control");
            4
        }))
        .run();

    println!("final (control) result: {:?}", result);
}
