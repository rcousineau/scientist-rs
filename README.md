# scientist-rs

Spiritual successor of [scientist](https://github.com/github/scientist) for Rust.

### Examples

There are some examples in the **examples** directory that can be run like so:

```sh
$ cargo run --example <example_name>
```
