extern crate scientist;

use crate::scientist::Experiment;

#[cfg(test)]
mod experiment_tests {
    use super::*;

    #[test]
    fn standard_experiment() {
        let result = Experiment::new()
            .candidate(Box::new(|| String::from("running candidate")))
            .control(Box::new(|| String::from("running control")))
            .run();

        assert_eq!(result, "running control");
    }

    #[test]
    fn multi_candidate_experiment() {
        let result = Experiment::new()
            .named_candidate(
                "candidate_1",
                Box::new(|| String::from("running candidate_1")),
            )
            .named_candidate(
                "candidate_2",
                Box::new(|| String::from("running candidate_2")),
            )
            .control(Box::new(|| String::from("running control")))
            .run();

        assert_eq!(result, "running control");
    }

    #[test]
    fn experiment_closing_over_inputs() {
        let a = 3;
        let b = 4;

        let result = Experiment::new()
            .candidate(Box::new(move || a * 2 + b))
            .control(Box::new(move || a * b))
            .run();

        assert_eq!(result, 12);
    }

    #[test]
    fn experiment_closing_over_heap_inputs() {
        let candidate_s = String::from("abc");
        // create an extra copy so that both can be moved
        let control_s = candidate_s.clone();

        let result = Experiment::new()
            .candidate(Box::new(move || {
                let mut updated = candidate_s.clone();
                updated.push_str("def");
                updated
            }))
            .control(Box::new(move || {
                let mut updated = control_s.clone();
                updated.push_str("xyz");
                updated
            }))
            .run();

        assert_eq!(result, "abcxyz");
    }
}
