use std::collections::HashMap;
use std::fmt::Debug;

type Behavior<T> = Box<dyn Fn() -> T>;

pub struct Experiment<T: Clone + Debug + PartialEq> {
    behaviors: HashMap<String, Behavior<T>>,
}

impl<T: Clone + Debug + PartialEq> Experiment<T> {
    pub fn new() -> Experiment<T> {
        Experiment {
            behaviors: HashMap::new(),
        }
    }

    pub fn named_candidate(&mut self, name: &str, behavior: Behavior<T>) -> &mut Experiment<T> {
        self.behaviors.insert(name.to_string(), behavior);

        self
    }

    pub fn candidate(&mut self, behavior: Behavior<T>) -> &mut Experiment<T> {
        self.named_candidate("candidate", behavior)
    }

    pub fn control(&mut self, behavior: Behavior<T>) -> &mut Experiment<T> {
        self.named_candidate("control", behavior)
    }

    pub fn run(&self) -> T {
        let mut results = HashMap::new();
        for (name, behavior) in &self.behaviors {
            results.insert(name, behavior());
        }

        let result = results
            .get(&String::from("control"))
            .expect("could not find 'control' results!")
            .clone();

        for (name, candidate_result) in results {
            if name != "control" && result != candidate_result {
                println!("{} results do not match: {:?}", name, candidate_result);
            }
        }

        result
    }
}
